# Autotest Monitoring

Service that monitors online services via sending auto-test to prod

## Table of contents

<details open="open">
  <summary>Content</summary>
  <ol>
    <li>
      <a href="#about-the-project">About project</a>
      <ol>
        <li><a href="#stack">Stack</a></li>
        <li><a href="#cache">Cache</a></li>
        <li><a href="#notes">Notes</a></li>
        <li><a href="#useful-links">Useful links</a></li>
      </ol>
    </li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#config">Config</a></li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#for-testers">For testers</a></li>
    <li><a href="#troubleshooting">Troubleshooting</a></li>
    <li><a href="#faq">FAQ</a></li>
    <li><a href="#maintainers">Maintainers</a></li>
    <li><a href="#todo">TODO</a></li>
  </ol>
</details>

## About The Project

Service collects photos of users.

### Stack

```
aiohttp==3.8.1
environs==9.5.0
aiogram==2.19
aioschedule==0.5.2
requests==2.27.1
```

### Cache

- None

### Notes

- Bot shows information about every source
- Has some manual handlers that triggers checking engine
- Runs cron at 08:00 and 15:00 to check all sources

### Useful links

According to [bot requirements](https://gitlab.argus360.kz/argus/analytics/-/issues/450).

## Installation

The project is made using docker containers

1. Fill all required environment variables (`.env` file)
2. Create python [virtual environment](https://docs.python.org/3/library/venv.html)
3. Download all required libraries `pip3 install -r requirements.txt`
4. Start project `python3 src/main.py`

## Config

List of all required envs. All envs must be set before build.

```
TELEGRAM_TOKEN=
ADMIN_IDS=
CHANNEL_ID=
LOG_LEVEL=

WEBINT_USER=
WEBINT_USER_PASS=
GRANT_TYPE=
CLIENT_ID=

ENV=
KEYCLOAK_PROD_URL=
KEYCLOAK_DEV_URL=

BASE_URL_DEV=
BASE_URL_PROD=
```

## Usage

First create a bot in BotFather, then add your bot to a channel and specify the id of this channel in the .env file.

- link to BotFather - https://t.me/botfather
- how to get channel id - https://stackoverflow.com/questions/33858927/how-to-obtain-the-chat-id-of-a-private-telegram-channel

### Response example

<ul>
    <li>{Source} - название источника, например Facebook/vk/ и т.д
        <ul>user_self - ✔</ul>
        <ul>user_friend - ✔</ul>
        <ul>user_following - ✔</ul>
        <ul>user_follower - ✔</ul>
        <ul>... - ✔</ul>
        <ul>post_owner_of - Х 500 InternalServerError</ul>
        <ul>group_following - Х IN PROGRESS</ul>
        <ul>group_self - EMPTY DATA</ul>
    </li>
</ul>

[//]: # (## Scheme Of Interaction)

## For testers
If you want to add a new service (resource) to the bot, follow these steps.
1. Git clone the project to your local machine
2. At `src/services/` level create new folder with source name
3. In this folder create 2 files `__init__.py and test_{source_name}.py` in `{source_name}` write your source name
4. In `test_{source_name}.py` add this imports
```import inspect

from services.utils.request_utility import default_request_utility as request_utility
from services.utils.utils import check_response

service_name = '{source_name}'
```
5. In `test_{source_name}.py` create `Test{service_name}` class
6. For every function in your `Test{service_name}` class add: 
   - decorator `@staticmethod`
   - add `async` prefix to make your function asynchronous
   - in every function add `function_name = inspect.stack()[0][3]`
   - add `response = await request_utility.get`
   - and at the end add `check_response(response, function_name, service_name)`
7. Out of class create new variable `test_{source_name} = Test{source_name}`
8. In `__init__.py` file add line `from .test_{source_name}(filename 3 step) import test_{source_name}(variable 6.5 step)`

Example:
```python
import inspect
from services.utils.request_utility import default_request_utility as request_utility
from services.utils.utils import check_response

service_name = 'Instagram'


class TestInstagram:
    @staticmethod
    async def user_self_screenname():
        function_name = inspect.stack()[0][3]
        response = await request_utility.get(
            resource='instagram',
            entity='user',
            relation='self',
            search_type='screenname',
            search_value='cristiano'
        )
        return check_response(response, function_name, service_name)
```

## Troubleshooting

- Contact to @qfpeeeer

## FAQ

No questions asked yet

## Maintainers

Current maintainers:

* Developed by @qfpeeeer
* Analytics by @dan_s

## TODO

- Feedback from testers
