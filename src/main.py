from aiogram import executor

from bot import dp
from conf import SKIP_UPDATES
from core.on_startup import on_startup, on_shutdown

if __name__ == '__main__':
    # test
    executor.start_polling(dp, on_startup=on_startup, on_shutdown=on_shutdown, skip_updates=SKIP_UPDATES)
