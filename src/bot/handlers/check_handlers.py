import os
from urllib.parse import urlparse

from aiogram import types
from whois import whois

from core import dp
from utils import Logger, filter_handler_args

logger = Logger(name='check_handlers')


async def who_is_domain(target_url, chat_id):
    logger.info(f'f[who_is_domain] -> target_ur: {target_url}, chat_id: {chat_id}')
    domain = urlparse(target_url)
    whois_domain = whois(domain.netloc)
    domain_text = whois_domain.text

    filename = f'whois_{domain.netloc}.txt'
    os.makedirs('whois_results', exist_ok=False)
    with open('whois_results/' + filename, 'w+') as f:
        f.write(domain_text)
    user_text = domain_text.split('>>>')
    if domain_text == 'Socket not responding':
        await dp.bot.send_message(
            chat_id=chat_id,
            text='<b>TLD not supported\n</b>Please contact Admins\n@MREX11 @AlibekCS @xjrfut',
            parse_mode="html"
        )
    else:
        try:
            await dp.bot.send_message(
                chat_id=chat_id,
                text=user_text[0],
                parse_mode="html"
            )
            await dp.send_document(chat_id, open(filename, 'rb'))
        except Exception as error:
            await dp.bot.send_message(
                chat_id=chat_id,
                text=f"""
                    {error}\n
                    Please contact Admins\n 
                    @MREX11 @AlibekCS @xjrfut""",
                parse_mode="html"
            )


@dp.message_handler(commands=["whois"])
async def start_handler(message: types.Message):
    args = filter_handler_args(message.get_args())
    logger.info("[ Command ] %s use [ whois_handler ]", message.from_user.username)

    if len(args) > 0:
        await who_is_domain(target_url=args[0], chat_id=message.from_user.id)
    else:
        await dp.bot.send_message(chat_id=message.from_user.id,
                                  text=f'Please input your url, for example: [/whois url]')
