from environs import Env

env = Env()

app_name = 'c0rx_bot'
# BOT
SKIP_UPDATES = False
TELEGRAM_TOKEN = env('TELEGRAM_TOKEN')

# ADMINS LIST
ADMIN_IDS = env.list('ADMIN_IDS')

# CHANNEL ID
CHANNEL_ID = env('CHANNEL_ID')

LOG_LEVEL = env('LOG_LEVEL')
