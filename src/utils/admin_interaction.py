from asyncio import sleep

from aiogram import Dispatcher
from aiogram.utils.exceptions import ChatNotFound
from utils import Logger

from conf import ADMIN_IDS

logger = Logger(name='admin_interaction')


async def notify_admin(dp: Dispatcher, notify_text):
    """
    :param notify_text:
    :type dp: object
    success start/restart notification
    """
    logger.info("Оповещение администрации...")
    for admin_id in ADMIN_IDS:
        try:
            await dp.bot.send_message(chat_id=admin_id, text=notify_text, disable_notification=True)
            logger.debug(f"Сообщение отправлено {admin_id}")
        except ChatNotFound:
            logger.debug("Чат с админом не найден")
        await sleep(0.3)
