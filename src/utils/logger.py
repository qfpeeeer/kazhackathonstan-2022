import logging
from datetime import datetime
from conf import LOG_LEVEL, app_name

log_level_map = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
}


class Logger:
    def __init__(self, level: int = log_level_map.get(LOG_LEVEL, 'info'), name: str = __name__):
        logging.basicConfig(level=level, format="")
        self._logger = logging.getLogger(name=name)
        self.name = name

    def set_level(self, level: int = logging.INFO):
        self._logger.setLevel(level=level)

    def error(self, msg: str, name: str = None, **kwargs):
        self._log(msg=msg, impl=self._logger.error, name=name if name is not None else self.name, **kwargs)

    def warning(self, msg: str, name: str = None, **kwargs):
        self._log(msg=msg, impl=self._logger.warning, name=name if name is not None else self.name, **kwargs)

    def info(self, msg: str, name: str = None, **kwargs):
        self._log(msg=msg, impl=self._logger.info, name=name if name is not None else self.name, **kwargs)

    def debug(self, msg, name: str = None, **kwargs):
        self._log(msg=msg, impl=self._logger.debug, name=name if name is not None else self.name, **kwargs)

    @staticmethod
    def _log(msg: str, impl, name, **kwargs):
        kwargs.update({
            "app_name": app_name,
            "time": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S'),
            "name": name,
            "level": impl.__name__,
            "msg": msg,
        })
        impl(kwargs)
