import asyncio

from aiogram import Dispatcher

from utils import Logger, notify_admin

logger = Logger(name='on_startup')


async def on_startup(dispatcher: Dispatcher):
    await notify_admin(dispatcher, "Bot launched successfully")
    logger.info("Bot launched successfully...")


async def on_shutdown(dispatcher: Dispatcher):
    await notify_admin(dispatcher, "Bot successfully disabled")
    logger.info("Bot successfully disabled...")
