from aiogram import Bot, Dispatcher, types

from conf import TELEGRAM_TOKEN

bot = Bot(token=TELEGRAM_TOKEN, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot)
